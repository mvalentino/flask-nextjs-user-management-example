import React, { Component } from "react";

export default class ComponentPage extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-12">
          <h1>Another Page</h1>
        </div>
      </div>
    );
  }
}
