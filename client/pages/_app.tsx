import App, { Container as NextContainer } from "next/app";
import Router from "next/router";
import NProgress from "nprogress";
import { Header, Footer } from "components/layout";
import { ErrorBoundary } from "components/error-boundary";
import nextCookie from "next-cookies";
import { ProvideAuth, AuthToken, TOKEN_STORAGE_NAME } from "utils/auth";
import { Container } from "reactstrap";

export default class CustomApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    const cookie = nextCookie(ctx);
    if (cookie[TOKEN_STORAGE_NAME]) {
      // @ts-ignore
      pageProps.auth = new AuthToken(cookie[TOKEN_STORAGE_NAME]);
    }
    return { pageProps };
  }

  componentDidMount() {
    Router.events.on("routeChangeComplete", () => {
      NProgress.start();
    });

    Router.events.on("routeChangeComplete", () => NProgress.done());
    Router.events.on("routeChangeError", () => NProgress.done());
  }

  componentDidCatch(error: any, errorInfo: any) {
    super.componentDidCatch(error, errorInfo);
  }

  render() {
    const { Component, pageProps } = this.props;

    const { auth } = pageProps;

    return (
      <NextContainer>
        <ProvideAuth auth={auth}>
          <Header />
        </ProvideAuth>
        <ErrorBoundary>
          <Container className="mt-3">
            <Component {...pageProps} />
          </Container>
        </ErrorBoundary>
        <Footer />
      </NextContainer>
    );
  }
}
